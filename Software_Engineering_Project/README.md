# Running The Program
As far as we can tell, 
we aren't suppsoed to create a
runnable jar-file, and instead instruct 
the user to run the program from: 
`src.main.java.business.projectManagementApp`,
where-in the main method is located.
To log into the program, you need a username. 
There are two available users: an admin,
and an employee. Find the usernames below.
To run our Cucumber tests, navigate to 
the tests folder in src/java and run the file 
'AcceptanceTests.java' in the Cucumber folder.
To run our jUnit-tests, instead of going to the
Cucumber folder, select the jUnit folder and 
'run with coverage' or select a specific test.


The program was written using 
IntelliJ Idea.
# Logging In
To login, you simply run the program, 
and type out one of the two following usernames, `qwer` or `huba`. 
The two users provide different user-experiences, as one is an admin,
and the other is not.
It is recommended to try both.
### Admin:
`Enter username:` `qwer`
### Employee:
`Enter username:` `huba`



